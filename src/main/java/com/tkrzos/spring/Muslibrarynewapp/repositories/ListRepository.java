package com.tkrzos.spring.Muslibrarynewapp.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ListRepository<TItem> {
    Page<TItem> list(Pageable pageable);
}
