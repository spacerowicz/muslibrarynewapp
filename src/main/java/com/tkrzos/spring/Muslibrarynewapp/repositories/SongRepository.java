package com.tkrzos.spring.Muslibrarynewapp.repositories;

import com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.song.SongListItemDTO;
import com.tkrzos.spring.Muslibrarynewapp.model.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SongRepository extends
        CrudRepository<Song, Long>,
        ListRepository<SongListItemDTO> {

    @Query("SELECT NEW com.tkrzos.spring.Muslibrarynewapp.dto.song.SongListItemDTO" +
            "(s.id, s.title, s.genre, s.year) " +
            "FROM com.tkrzos.spring.Muslibrarynewapp.model.Song s")
    Page<SongListItemDTO> list(Pageable pageable);

    @Query("SELECT NEW com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO(s.id, s.title) " +
            "FROM com.tkrzos.spring.Muslibrarynewapp.model.Song s " +
            "INNER JOIN s.artists a " +
            "WHERE a.id = :artistId " +
            "ORDER BY s.title")
    List<IdNamePairDTO> findIdNamePairsByArtistId(@Param("artistId") Long artistId);
}