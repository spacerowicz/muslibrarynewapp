package com.tkrzos.spring.Muslibrarynewapp.repositories;

import com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.artist.ArtistListItemDTO;
import com.tkrzos.spring.Muslibrarynewapp.model.Artist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArtistRepository extends
        CrudRepository<Artist, Long>,
        ListRepository<ArtistListItemDTO> {
    @Query("SELECT NEW com.tkrzos.spring.Muslibrarynewapp.dto.artist.ArtistListItemDTO(a.id, a.nick) " +
            "FROM com.tkrzos.spring.Muslibrarynewapp.model.Artist a")
    Page<ArtistListItemDTO> list(Pageable pageable);

    @Query("SELECT NEW com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO(a.id, CONCAT(a.lastName, ' ', a.firstName, '(', a.nick, ')')) " +
            "FROM com.tkrzos.spring.Muslibrarynewapp.model.Artist a " +
            "ORDER BY a.lastName")
    List<IdNamePairDTO> findIdNamePairs();

    @Query("SELECT NEW com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO(a.id, CONCAT(a.lastName, ' ', a.firstName, '(', a.nick, ')')) " +
            "FROM com.tkrzos.spring.Muslibrarynewapp.model.Artist a " +
            "INNER JOIN a.songs s " +
            "WHERE s.id = :songId " +
            "ORDER BY a.lastName")
    List<IdNamePairDTO> findIdNamePairsBySongId(@Param("songId") Long songId);
}