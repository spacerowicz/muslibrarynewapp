package com.tkrzos.spring.Muslibrarynewapp;

import com.google.common.collect.Iterables;
import com.tkrzos.spring.Muslibrarynewapp.model.Artist;
import com.tkrzos.spring.Muslibrarynewapp.model.Song;
import com.tkrzos.spring.Muslibrarynewapp.repositories.ArtistRepository;
import com.tkrzos.spring.Muslibrarynewapp.repositories.SongRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SpringBootApplication
public class MuslibrarynewappApplication {
	private ArtistRepository artistRepository;
	private SongRepository songRepository;

	public MuslibrarynewappApplication(ArtistRepository artistRepository, SongRepository songRepository) {
		this.artistRepository = artistRepository;
		this.songRepository = songRepository;
	}

	@EventListener(ApplicationReadyEvent.class)
	private void loadData() {
		String[][] artists = new String[][] {
				// First name, last name, nick
				{ "Jan", "Kowalski", "Kowal" },
				{ "Marcin", "Nowak", "Nowy" },
				{ "Marian", "Sokół", "Sokół" },
                { "Martyna", "Zalewska", "Cytryna" },
                { "Marta", "Szczypiorek", "Szczypior" },

                { "Joanna", "Szlachta", "Szlachcic" },
                { "Andrzej", "Malinowski", "Malina" },
                { "Jakub", "Dąbrowski", "Dąb" },
                { "Czesław", "Zioło", "Ziółko" },
                { "Paweł", "Kasza", "Kasza" },

                { "Zdzisław", "Fasola", "Fasola" },
		};

        String[][] songs = new String[][] {
                // Title, genre, year, recordLabel
                { "Jedźmy gdzieś!", "Disco Polo", "1990", "Wytwornia A" },
                { "Szept", "Rock", "2010", "Wytwornia B" },
                { "Kolorowe życie", "Pop", "2005", "Wytwornia C" },
                { "Słodkich snów", "Rock", "2001", "Wytwornia C" },
                { "Biała róża", "Pop", "2014", "Wytwornia A" },

                { "Polarny miś", "Disco Polo", "2000", "Wytwornia B" },
                { "Street", "Hip-Hop", "2005", "Wytwornia B" },
                { "Żaglówka", "Dance", "2012", "Wytwornia B" },
                { "Emigracja", "Hip-Hop", "2010", "Wytwornia C" },
                { "Radosne życie", "Pop", "2018", "Wytwornia C" },

                { "Marzenia", "Rock", "2018", "Wytwornia A" },
        };

		List<Artist> artistEntities = new ArrayList<>();

		for (String[] a: artists) {
			Artist artist = new Artist();
			artist.setFirstName(a[0]);
			artist.setLastName(a[1]);
			artist.setNick(a[2]);
			artistEntities.add(artist);
		}

		artistRepository.saveAll(artistEntities);

		List<Song> songEntities = new ArrayList<>();

        Iterator<Artist> artistIterator = Iterables.cycle(artistEntities).iterator();

		for (String[] s: songs) {
		    Artist artist = artistIterator.next();

			Song song = new Song();
			song.setTitle(s[0]);
			song.setGenre(s[1]);
			song.setYear(Integer.parseInt(s[2]));
			song.setRecordLabel(s[3]);
            song.getArtists().add(artist);
			songEntities.add(song);
		}

		songRepository.saveAll(songEntities);
	}

	public static void main(String[] args) {
		SpringApplication.run(MuslibrarynewappApplication.class, args);
	}
}
