package com.tkrzos.spring.Muslibrarynewapp.service.impl;

import com.google.common.collect.Sets;
import com.tkrzos.spring.Muslibrarynewapp.dto.CheckboxDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.song.*;
import com.tkrzos.spring.Muslibrarynewapp.model.Artist;
import com.tkrzos.spring.Muslibrarynewapp.model.Song;
import com.tkrzos.spring.Muslibrarynewapp.repositories.ArtistRepository;
import com.tkrzos.spring.Muslibrarynewapp.repositories.SongRepository;
import com.tkrzos.spring.Muslibrarynewapp.service.SongService;
import com.tkrzos.spring.Muslibrarynewapp.utlis.ServiceUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;

@Service
public class SongServiceImpl implements SongService {
    private SongRepository songRepository;
    private ArtistRepository artistRepository;

    public SongServiceImpl(SongRepository songRepository, ArtistRepository artistRepository) {
        this.songRepository = songRepository;
        this.artistRepository = artistRepository;
    }

    @Override
    @Transactional
    public Long createSong(CreateSongDTO song) {
        Assert.notNull(song, "Song cannot be null");

        List<Long> artistIds = new ArrayList<>();

        if (song.getArtists() != null) {
            for (CheckboxDTO ch : song.getArtists()) {
                Assert.notNull(ch.getId(), "Artist id cannot be null");
                if (ch.getChecked()) {
                    artistIds.add(ch.getId());
                }
            }
        }

        Set<Artist> artists = !artistIds.isEmpty() ?
                Sets.newHashSet(artistRepository.findAllById(artistIds)) :
                new HashSet<>();

        Song songEntity = new Song(song.getTitle(), song.getGenre(), song.getYear(), song.getRecordLabel(), artists);
        songRepository.save(songEntity);
        return songEntity.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public CreateSongDTO createSongForm() {
        CreateSongDTO result = new CreateSongDTO();
        List<IdNamePairDTO> pairList = artistRepository.findIdNamePairs();
        List<CheckboxDTO> artists = new ArrayList<>();

        for (IdNamePairDTO p: pairList) {
            artists.add(new CheckboxDTO(p.getId(), p.getName()));
        }

        result.setArtists(artists);
        return result;
    }

    @Override
    @Transactional
    public void editSong(EditSongDTO song) {
        Assert.notNull(song, "Song cannot be null");
        Assert.notNull(song.getId(), "Song id cannot be null");

        Optional<Song> songEntity = songRepository.findById(song.getId());
        Assert.state(songEntity.isPresent(), String.format("Song #%d not found", song.getId()));

        List<Long> artistIds = new ArrayList<>();

        if (song.getArtists() != null) {
            for (CheckboxDTO ch : song.getArtists()) {
                Assert.notNull(ch.getId(), "Id cannot be null");
                if (ch.getChecked()) {
                    artistIds.add(ch.getId());
                }
            }
        }

        Set<Artist> artists = !artistIds.isEmpty() ?
                Sets.newHashSet(artistRepository.findAllById(artistIds)) :
                new HashSet<>();

        songEntity.get().setTitle(song.getTitle());
        songEntity.get().setGenre(song.getGenre());
        songEntity.get().setYear(song.getYear());
        songEntity.get().setRecordLabel(song.getRecordLabel());
        songEntity.get().setArtists(artists);

        songRepository.save(songEntity.get());
    }

    @Override
    @Transactional(readOnly = true)
    public EditSongDTO editSongForm(Long id) {
        Assert.notNull(id, "Song id cannot be null");

        Optional<Song> songEntity = songRepository.findById(id);
        Assert.state(songEntity.isPresent(), String.format("Song #%d not found", id));

        EditSongDTO result = new EditSongDTO();
        result.setId(songEntity.get().getId());
        result.setTitle(songEntity.get().getTitle());
        result.setGenre(songEntity.get().getGenre());
        result.setRecordLabel(songEntity.get().getRecordLabel());
        result.setYear(songEntity.get().getYear());
        
        List<IdNamePairDTO> artistList = artistRepository.findIdNamePairs();
        List<IdNamePairDTO> checkedArtistList = artistRepository.findIdNamePairsBySongId(id);
        Set<Long> checkedArtistIds = new HashSet<>();
        List<CheckboxDTO> checkboxes = new ArrayList<>();

        for (IdNamePairDTO p: checkedArtistList) {
            checkedArtistIds.add(p.getId());
        }

        for (IdNamePairDTO p: artistList) {
            checkboxes.add(new CheckboxDTO(p.getId(), p.getName(),
                    checkedArtistIds.contains(p.getId()) ? true : false ));
        }

        result.setArtists(checkboxes);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public SongDetailsDTO songDetails(Long id) {
        Assert.notNull(id, "Song id cannot be null");

        Optional<Song> songEntity = songRepository.findById(id);
        Assert.state(songEntity.isPresent(), String.format("Song #%d not found", id));

        SongDetailsDTO result = new SongDetailsDTO();
        result.setId(songEntity.get().getId());
        result.setTitle(songEntity.get().getTitle());
        result.setGenre(songEntity.get().getGenre());
        result.setRecordLabel(songEntity.get().getRecordLabel());
        result.setYear(songEntity.get().getYear());
        result.setArtists(artistRepository.findIdNamePairsBySongId(id));

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SongListItemDTO> songList(Pageable pageable) {
        return ServiceUtils.list(songRepository, pageable);
    }

    @Override
    @Transactional
    public void deleteSong(Long id) {
        Assert.notNull(id, "Song id cannot be null");
        songRepository.deleteById(id);
    }
}
