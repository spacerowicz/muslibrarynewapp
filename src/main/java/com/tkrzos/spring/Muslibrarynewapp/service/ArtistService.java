package com.tkrzos.spring.Muslibrarynewapp.service;

import com.tkrzos.spring.Muslibrarynewapp.dto.artist.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArtistService {
    Long createArtist(CreateArtistDTO artist);
    void editArtist(EditArtistDTO artist);
    EditArtistDTO editArtistForm(Long id);
    ArtistDetailsDTO artistDetails(Long id);
    Page<ArtistListItemDTO> artistList(Pageable pageable);
    void deleteArtist(Long id);
}
