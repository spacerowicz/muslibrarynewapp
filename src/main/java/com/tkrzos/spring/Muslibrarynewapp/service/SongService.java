package com.tkrzos.spring.Muslibrarynewapp.service;

import com.tkrzos.spring.Muslibrarynewapp.dto.song.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SongService {
    Long createSong(CreateSongDTO song);
    CreateSongDTO createSongForm();

    void editSong(EditSongDTO song);
    EditSongDTO editSongForm(Long id);

    SongDetailsDTO songDetails(Long id);
    Page<SongListItemDTO> songList(Pageable pageable);
    void deleteSong(Long id);
}
