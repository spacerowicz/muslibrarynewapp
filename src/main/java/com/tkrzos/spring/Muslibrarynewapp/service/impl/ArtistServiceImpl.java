package com.tkrzos.spring.Muslibrarynewapp.service.impl;

import com.tkrzos.spring.Muslibrarynewapp.dto.artist.*;
import com.tkrzos.spring.Muslibrarynewapp.model.Artist;
import com.tkrzos.spring.Muslibrarynewapp.repositories.ArtistRepository;
import com.tkrzos.spring.Muslibrarynewapp.repositories.SongRepository;
import com.tkrzos.spring.Muslibrarynewapp.service.ArtistService;
import com.tkrzos.spring.Muslibrarynewapp.utlis.ServiceUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Optional;

@Service
public class ArtistServiceImpl implements ArtistService {
    private ArtistRepository artistRepository;
    private SongRepository songRepository;

    public ArtistServiceImpl(ArtistRepository artistRepository, SongRepository songRepository) {
        this.artistRepository = artistRepository;
        this.songRepository = songRepository;
    }

    @Override
    @Transactional
    public Long createArtist(CreateArtistDTO artist) {
        Assert.notNull(artist, "Artist cannot be null");

        Artist artistEntity = new Artist(
                artist.getFirstName(),
                artist.getLastName(),
                artist.getNick());

        artistRepository.save(artistEntity);
        return artistEntity.getId();
    }

    @Override
    @Transactional
    public void editArtist(EditArtistDTO artist) {
        Assert.notNull(artist, "Artist cannot be null");
        Assert.notNull(artist.getId(), "Artist id cannot be null");

        Optional<Artist> artistEntity = artistRepository.findById(artist.getId());
        Assert.state(artistEntity.isPresent(), String.format("Artist #%d not found", artist.getId()));

        artistEntity.get().setFirstName(artist.getFirstName());
        artistEntity.get().setLastName(artist.getLastName());
        artistEntity.get().setNick(artist.getNick());

        artistRepository.save(artistEntity.get());
    }

    @Override
    @Transactional(readOnly = true)
    public EditArtistDTO editArtistForm(Long id) {
        Assert.notNull(id, "Artist id cannot be null");

        Optional<Artist> artistEntity = artistRepository.findById(id);
        Assert.state(artistEntity.isPresent(), String.format("Artist #%d not found", id));

        EditArtistDTO result = new EditArtistDTO();
        result.setId(artistEntity.get().getId());
        result.setNick(artistEntity.get().getNick());
        result.setFirstName(artistEntity.get().getFirstName());
        result.setLastName(artistEntity.get().getLastName());

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public ArtistDetailsDTO artistDetails(Long id) {
        Assert.notNull(id, "Artist id cannot be null");

        Optional<Artist> artist = artistRepository.findById(id);
        Assert.state(artist.isPresent(), String.format("Artist #%d not found", id));

        ArtistDetailsDTO result = new ArtistDetailsDTO();
        result.setId(artist.get().getId());
        result.setNick(artist.get().getNick());
        result.setFirstName(artist.get().getFirstName());
        result.setLastName(artist.get().getLastName());
        result.setSongs(songRepository.findIdNamePairsByArtistId(id));

        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ArtistListItemDTO> artistList(Pageable pageable) {
        return ServiceUtils.list(artistRepository, pageable);
    }

    @Override
    @Transactional
    public void deleteArtist(Long id) {
        Assert.notNull(id, "Artist id cannot be null");
        artistRepository.deleteById(id);
    }
}
