package com.tkrzos.spring.Muslibrarynewapp.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "songs")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 50)
    private String title;

    @NotNull
    @NotBlank
    @Size(max = 50)
    private String genre;

    @NotNull
    private Integer year;

    @NotNull
    @NotBlank
    @Size(max = 50)
    private String recordLabel;

    @ManyToMany
    @JoinTable(name = "artist_song",
            joinColumns = @JoinColumn(name = "song_id"),
            inverseJoinColumns = @JoinColumn(name="artist_id"))
    private Set<Artist> artists = new HashSet<>();

    public Song() {
    }

    public Song(String title, String genre, Integer year, String recordLabel) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.recordLabel = recordLabel;
    }

    public Song(String title, String genre, Integer year, String recordLabel, Set<Artist> artists) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.recordLabel = recordLabel;
        this.artists = artists;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getRecordLabel() {
        return recordLabel;
    }

    public void setRecordLabel(String recordLabel) {
        this.recordLabel = recordLabel;
    }

    public Set<Artist> getArtists() {
        return artists;
    }

    public void setArtists(Set<Artist> artists) {
        this.artists = artists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Objects.equals(id, song.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Song{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", year='" + year + '\'' +
                ", recordLabel='" + recordLabel + '\'' +
                ", artists=" + artists +
                '}';
    }
}
