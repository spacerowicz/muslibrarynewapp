package com.tkrzos.spring.Muslibrarynewapp.utlis;

import com.tkrzos.spring.Muslibrarynewapp.repositories.ListRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class ServiceUtils {
    public static <TItem> Page<TItem> list(ListRepository repository, Pageable pageable) {
        Page<TItem> result = repository.list(pageable);

        if (result.hasContent() == false && result.hasPrevious() && result.getTotalPages() > 0) {
            Pageable pageRequest = PageRequest.of(
                    result.getTotalPages() - 1,
                    pageable.getPageSize(),
                    pageable.getSort());
            return repository.list(pageRequest);
        }

        return result;
    }
}