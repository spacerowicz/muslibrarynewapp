package com.tkrzos.spring.Muslibrarynewapp.controller;

import com.tkrzos.spring.Muslibrarynewapp.dto.song.CreateSongDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.song.EditSongDTO;
import com.tkrzos.spring.Muslibrarynewapp.service.SongService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SongController {
    private static final Logger logger = Logger.getLogger(SongController.class);
    private SongService songService;

    public SongController(SongService songService) {
        this.songService = songService;
    }

    @RequestMapping("/songs")
    public String index(
            Model model,
            @PageableDefault(size = 5, page = 0, sort = "title") Pageable pageable) {
        Sort.Order orderById = Sort.Order.by("id");
        Sort.Order ignoreCaseOrderByTitle = pageable.getSort().getOrderFor("title").ignoreCase();
        Pageable ignoreCasePageable = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by(ignoreCaseOrderByTitle, orderById)
        );
        model.addAttribute("page", songService.songList(ignoreCasePageable));
        return "song/index";
    }

    @RequestMapping("/songs/details")
    public String details(Model model, @RequestParam("id") Long id) {
        model.addAttribute("data", songService.songDetails(id));
        return "song/details";
    }

    @RequestMapping("/songs/add")
    public String add(Model model) {
        model.addAttribute("showError", false);
        model.addAttribute("song", songService.createSongForm());
        return "song/add";
    }

    @RequestMapping(value = "/songs/add", method = RequestMethod.POST)
    public String addPost(
            Model model,
            CreateSongDTO song,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            songService.createSong(song);
            return redirectToList(page);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            model.addAttribute("showError", true);
            model.addAttribute("song", song);
            return "song/add";
        }
    }

    @RequestMapping("/songs/edit")
    public String edit(Model model, @RequestParam("id") Long id) {
        model.addAttribute("showError", false);
        model.addAttribute("song", songService.editSongForm(id));
        return "song/edit";
    }

    @RequestMapping(value = "/songs/edit", method = RequestMethod.POST)
    public String editPost(
            Model model,
            EditSongDTO song,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            songService.editSong(song);
            return redirectToList(page);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            model.addAttribute("showError", true);
            model.addAttribute("song", song);
            return "song/edit";
        }
    }

    @RequestMapping("/songs/delete")
    public String delete(
            @RequestParam("id") Long id,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            songService.deleteSong(id);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return redirectToList(page);
    }

    private String redirectToList(int page) {
        return "redirect:/songs?page=" + page;
    }
}