package com.tkrzos.spring.Muslibrarynewapp.controller;

import com.tkrzos.spring.Muslibrarynewapp.dto.artist.CreateArtistDTO;
import com.tkrzos.spring.Muslibrarynewapp.dto.artist.EditArtistDTO;
import com.tkrzos.spring.Muslibrarynewapp.service.ArtistService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ArtistController {
    private static final Logger logger = Logger.getLogger(ArtistController.class);
    private ArtistService artistService;

    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @RequestMapping("/artists")
    public String index(Model model, @PageableDefault(size = 5, page = 0, sort = "nick")  Pageable pageable) {
        Sort.Order orderById = Sort.Order.by("id");
        Sort.Order ignoreCaseOrderByNick = pageable.getSort().getOrderFor("nick").ignoreCase();
        Pageable ignoreCasePageable = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by(ignoreCaseOrderByNick, orderById)
        );
        model.addAttribute("page", artistService.artistList(ignoreCasePageable));
        return "artist/index";
    }

    @RequestMapping("/artists/details")
    public String details(Model model, @RequestParam("id") Long id) {
        model.addAttribute("data", artistService.artistDetails(id));
        return "artist/details";
    }

    @RequestMapping("/artists/add")
    public String add(Model model) {
        model.addAttribute("showError", false);
        model.addAttribute("artist", new CreateArtistDTO());
        return "artist/add";
    }

    @RequestMapping(value = "/artists/add", method = RequestMethod.POST)
    public String addPost(
            Model model,
            CreateArtistDTO artist,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            artistService.createArtist(artist);
            return redirectToList(page);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            model.addAttribute("showError", true);
            model.addAttribute("artist", artist);
            return "artist/add";
        }
    }

    @RequestMapping("/artists/edit")
    public String edit(Model model, @RequestParam("id") Long id) {
        model.addAttribute("showError", false);
        model.addAttribute("artist", artistService.editArtistForm(id));
        return "artist/edit";
    }

    @RequestMapping(value = "/artists/edit", method = RequestMethod.POST)
    public String editPost(
            Model model,
            EditArtistDTO artist,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            artistService.editArtist(artist);
            return redirectToList(page);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            model.addAttribute("showError", true);
            model.addAttribute("artist", artist);
            return "artist/edit";
        }
    }

    @RequestMapping("/artists/delete")
    public String delete(
            @RequestParam("id") Long id,
            @RequestParam(value = "page", defaultValue = "0") int page) {
        try {
            artistService.deleteArtist(id);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return redirectToList(page);
    }

    private String redirectToList(int page) {
        return "redirect:/artists?page=" + page;
    }
}