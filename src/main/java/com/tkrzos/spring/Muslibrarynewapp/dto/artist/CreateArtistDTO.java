package com.tkrzos.spring.Muslibrarynewapp.dto.artist;

import java.util.Objects;

public class CreateArtistDTO {
    private String firstName;
    private String lastName;
    private String nick;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateArtistDTO that = (CreateArtistDTO) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(nick, that.nick);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, nick);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CreateArtistDTO{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", nick='").append(nick).append('\'');
        sb.append('}');
        return sb.toString();
    }
}