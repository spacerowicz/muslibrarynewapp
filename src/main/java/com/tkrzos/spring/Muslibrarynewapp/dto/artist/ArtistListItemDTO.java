package com.tkrzos.spring.Muslibrarynewapp.dto.artist;

import java.util.Objects;

public class ArtistListItemDTO {
    private Long id;
    private String nick;

    public ArtistListItemDTO(Long id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtistListItemDTO that = (ArtistListItemDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nick, that.nick);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nick);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ArtistListItemDTO{");
        sb.append("id=").append(id);
        sb.append(", nick='").append(nick).append('\'');
        sb.append('}');
        return sb.toString();
    }
}