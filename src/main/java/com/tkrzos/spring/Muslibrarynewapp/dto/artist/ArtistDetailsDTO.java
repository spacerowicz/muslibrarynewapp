package com.tkrzos.spring.Muslibrarynewapp.dto.artist;

import com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArtistDetailsDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String nick;
    private List<IdNamePairDTO> songs = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<IdNamePairDTO> getSongs() {
        return songs;
    }

    public void setSongs(List<IdNamePairDTO> songs) {
        this.songs = songs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtistDetailsDTO that = (ArtistDetailsDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(nick, that.nick) &&
                Objects.equals(songs, that.songs);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, nick, songs);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ArtistDetailsDTO{");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", nick='").append(nick).append('\'');
        sb.append('}');
        return sb.toString();
    }
}