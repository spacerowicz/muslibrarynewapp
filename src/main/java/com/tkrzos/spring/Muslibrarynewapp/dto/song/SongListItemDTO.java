package com.tkrzos.spring.Muslibrarynewapp.dto.song;

import java.util.Objects;

public class SongListItemDTO {
    private Long id;
    private String title;
    private String genre;
    private Integer year;

    public SongListItemDTO(Long id, String title, String genre, Integer year) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongListItemDTO that = (SongListItemDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(genre, that.genre) &&
                Objects.equals(year, that.year);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, genre, year);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SongListItemDTO{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", genre='").append(genre).append('\'');
        sb.append(", year='").append(year).append('\'');
        sb.append('}');
        return sb.toString();
    }
}