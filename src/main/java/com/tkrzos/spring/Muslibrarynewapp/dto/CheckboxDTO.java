package com.tkrzos.spring.Muslibrarynewapp.dto;

import java.util.Objects;

public class CheckboxDTO {
    private Long id;
    private String label;
    private Boolean checked = false;

    public CheckboxDTO() {
    }

    public CheckboxDTO(Long id, String label) {
        this.id = id;
        this.label = label;
        this.checked = false;
    }

    public CheckboxDTO(Long id, String label, Boolean checked) {
        this.id = id;
        this.label = label;
        this.checked = checked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckboxDTO that = (CheckboxDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(label, that.label) &&
                Objects.equals(checked, that.checked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label, checked);
    }

    @Override
    public String toString() {
        return "CheckboxDTO{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", checked=" + checked +
                '}';
    }
}