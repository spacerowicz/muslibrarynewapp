package com.tkrzos.spring.Muslibrarynewapp.dto.song;

import com.tkrzos.spring.Muslibrarynewapp.dto.IdNamePairDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SongDetailsDTO {
    private Long id;
    private String title;
    private String genre;
    private Integer year;
    private String recordLabel;
    private List<IdNamePairDTO> artists = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getRecordLabel() {
        return recordLabel;
    }

    public void setRecordLabel(String recordLabel) {
        this.recordLabel = recordLabel;
    }

    public List<IdNamePairDTO> getArtists() {
        return artists;
    }

    public void setArtists(List<IdNamePairDTO> artists) {
        this.artists = artists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongDetailsDTO that = (SongDetailsDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(genre, that.genre) &&
                Objects.equals(year, that.year) &&
                Objects.equals(recordLabel, that.recordLabel) &&
                Objects.equals(artists, that.artists);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, genre, year, recordLabel, artists);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SongDetailsDTO{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", genre='").append(genre).append('\'');
        sb.append(", year='").append(year).append('\'');
        sb.append(", recordLabel='").append(recordLabel).append('\'');
        sb.append('}');
        return sb.toString();
    }
}